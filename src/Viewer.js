/**
 * Created by sanket.thanvi
 * 27 April 2019
 */
import { createAppContainer, createDrawerNavigator, createStackNavigator, } from 'react-navigation';
import React from 'react';
import { StyleSheet, Image, View, Text } from 'react-native';

import Home from './Home';
import Profile from './Profile';
import BookModal from './modals/BookModal'
import seed from './seed';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    fontWeight: "700",
  },
  icon: {
    width: 24,
    height: 24,
    marginVertical: 20
  },
});

const MyDrawerNavigator = createDrawerNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      drawerLabel: 'Home',
      drawerIcon: ({ tintColor }) => (
        <Image
          source={{ uri: 'https://cdn4.iconfinder.com/data/icons/wirecons-free-vector-icons/32/menu-alt-512.png'}}
          style={[styles.icon, {tintColor: tintColor}]}
        />
      ),
    },
  },
  Profile: {
    screen: Profile,
    navigationOptions: ({ screenProps }) => {
      return {
          drawerLabel: 'Profile',
          drawerIcon: () => (
            <Image
              source={{ uri: screenProps ? screenProps.viewer.profile.picture : 'https://img.icons8.com/dotty/2x/administrator-male.png' }}
              style={[styles.icon]}
            />
          ),
      }
    },
  },
});

const RootStack =  createStackNavigator({
  Main: {
    screen: MyDrawerNavigator
  },
  BookModal: {
    screen: BookModal
  }
},
{
  mode: 'modal',
  headerMode: 'none'
})

const ViewerTab = createAppContainer(RootStack);

// const config = {
// };

// const fire = firebase.initializeApp(config);

class Viewer extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      data: seed,
      showText: true,
    }
    // setInterval(() => {
    //     this.setState(previousState => {
    //       return { showText: !previousState.showText };
    //     });
    //   }, 
    // 500);
  }


  // readUserData = () => {
  //   fire.database().ref('user/').on('value', (snapshot) => {
  //     const data = snapshot.val();
  //     this.setState({
  //       data: {
  //         viewer: {
  //           ...data
  //         },
  //       },
  //     })
  //   });
  // }

  componentDidMount(){
    // this.readUserData();
  }

  render() {
    const { data, showText } = this.state;
    if (!data) {
      return (
        <View style={styles.container}>
          <Text style={{ fontSize: 20 }}>{showText? 'Loading data...' : ''}</Text>
        </View>
      )
    }

    return (
      <ViewerTab
        screenProps={{
          ...data,
          // fire,
          signOut: this.props.signOut
        }}
      />
    )
  }
}

export default Viewer;