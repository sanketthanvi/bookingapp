/**
 * Created by sanket.thanvi
 * 27 April 2019
 * @format
 * @flow
 */

 
import React from 'react';
import { Button, StyleSheet, View, Text } from 'react-native';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    fontWeight: "700",
  },
});


export default class Login extends React.Component {
  render() {

    const { afterLogin } = this.props;
    return (
      <View style={styles.container}>
        <Text style={{ fontWeight: '500', fontSize: 25, marginBottom: 30 }}>
          Booking App
        </Text>
        <View 
          style={{
            backgroundColor: "darkblue",
            marginBottom: 40,
          }}
        >
          <Button
            onPress={afterLogin}
            title="Login"
            color="white"
          />
        </View>
        <Text style={{ fontSize: 15, marginBottom: 10 }}>
          by Sanket Thanvi
        </Text>
        <Text style={{ fontSize: 15, marginBottom: 20 }}>
          for Tazweed Interview Test
        </Text>
      </View>
    );
  }
}
