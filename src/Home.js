import React from 'react';
import {
    createBottomTabNavigator,
    createAppContainer,
} from 'react-navigation';
import { StyleSheet, Image } from 'react-native';

import UpcomingScreen from './screens/UpcomingScreen';
import HistoryScreen from './screens/HistoryScreen';
import BookScreen from './screens/BookScreen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        fontWeight: "700",
    },
    icon: {
        width: 24,
        height: 24,
    },
});

const HomeTabContainer = createAppContainer(createBottomTabNavigator({
  Book: BookScreen,
  Upcoming: UpcomingScreen,
  History: HistoryScreen,
},
{
  tabBarOptions: {
    activeTintColor: 'darkblue',
    labelStyle: {
      fontSize: 16,
      fontWeight: '500'
    },
  }
}));

export default class Home extends React.Component {
  static navigationOptions = {
    drawerLabel: 'Home',
    drawerIcon: ({ tintColor }) => (
      <Image
        source={{ uri: 'https://cdn4.iconfinder.com/data/icons/wirecons-free-vector-icons/32/menu-alt-512.png'}}
        style={[styles.icon, {tintColor: tintColor}]}
      />
    ),
  };

  render() {
    return (
        <HomeTabContainer 
          screenProps={{
            navigation: this.props.navigation,
            ...this.props.screenProps,
          }}
        />
    );
  }
}


