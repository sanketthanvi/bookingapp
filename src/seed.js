/**
 * Created by sanket.thanvi
 * 27 April 2019
 */
export default {
    viewer: {
        profile: {
            name: 'Sanket Thanvi',
            mobile: '+91-8504005239',
            email: 'sanket.thanvi@gmail.com',
            picture: 'https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?resize=256%2C256&quality=100&ssl=1',            
        }
    }
}