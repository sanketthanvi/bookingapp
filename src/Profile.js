/**
 * Created by sanket.thanvi
 * 27 April 2019
 */
import React from 'react';
import { StyleSheet, Button, Image, View, Text } from 'react-native';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
      fontWeight: "700",
      flexDirection: 'column'
    },
    icon: {
      width: 120,
      height: 120,
    },
});

export default class Profile extends React.Component {

    render() {
      console.log('profile', this.props)
      const profile = this.props.screenProps.viewer.profile;
      const { picture, email, mobile, name } = profile;

      return (
        <View style={styles.container}>
          <Image
            source={{ uri: picture }}
            style={[styles.icon]}
          />
          <View style={{ marginVertical: 10, flexDirection: 'column' }}>
            <Text style={{ fontSize: 20 }}>
              {name}
            </Text>
          </View>
          <View style={{ marginVertical: 5, flexDirection: 'column' }}>
            <Text style={{ fontSize: 15 }}>
              {mobile}
            </Text>
          </View>
          <View style={{ marginVertical: 5, flexDirection: 'column', marginBottom: 10 }}>
            <Text style={{ fontSize: 15 }}>
              {email}
            </Text>
          </View>
          <View style={{ backgroundColor: 'purple' }}>
            <Button
              onPress={() => this.props.screenProps.signOut()}
              title="Sign Out"
              color="white"
            />
          </View>
        </View>
      );
    }
}
  