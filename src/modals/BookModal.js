/**
 * Created by sanket.thanvi
 * 27 April 2019
 */
import React from 'react';
import {
    createBottomTabNavigator,
    createAppContainer,
    createStackNavigator,
} from 'react-navigation';
import { 
    Button, 
    View, 
    Text,
} from 'react-native';
import DatePicker from 'react-native-datepicker';

export default class BookModal extends React.Component {
    state = {};

    render() {

        const { navigation } = this.props;
        const item = navigation.getParam('item');
        const { name, address, place, state } = item;
        return (
            <View 
                style={{ 
                    flex: 1,
                    paddingTop: 40,
                    flexDirection: 'column',
                }}
            >
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        borderBottomColor: 'black',
                        borderBottomWidth: 1,
                    }}
                >
                    <Button
                        title="Book"
                        onPress={() => {}}
                    />
                    <Text
                        style={{
                            flex: 1,
                            fontSize: 20,
                            fontWeight: '500',
                            textAlign: 'center'
                        }}
                    >
                        Book An Appointment
                    </Text>
                    <Button
                        title="Close"
                        color="red"
                        onPress={() => this.props.navigation.goBack()}
                    />
                </View>
                <View
                    style={{
                        flexDirection: 'column',
                        alignItems: 'center',
                        marginVertical: 10,
                        justifyContent: 'center',
                    }}
                >
                    <Text
                        style={{
                            fontSize: 20,
                            marginHorizontal: 20,
                            textAlign: 'center',
                            textAlignVertical: 'center',
                            fontWeight: 'bold',
                            marginBottom: 5,
                        }}
                    >
                        {name}
                    </Text>
                    <Text
                        style={{
                            fontSize: 15,
                            marginHorizontal: 5,
                            textAlign: 'center',
                            textAlignVertical: 'center',
                        }}
                    >
                        {address}
                    </Text>
                    <Text
                        style={{
                            fontSize: 15,
                            marginHorizontal: 5,
                            textAlign: 'center',
                            textAlignVertical: 'center',
                            marginBottom: 15,
                        }}
                    >
                        {place}, {state}
                    </Text>
                </View>
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'column',
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'flex-start',
                            marginVertical: 10,
                            justifyContent: 'center',
                        }}
                    >
                        <Text
                            style={{
                                fontSize: 15,
                                marginHorizontal: 5,
                                textAlign: 'center',
                                textAlignVertical: 'center',
                                marginBottom: 10,
                            }}
                        >
                            Please select data and time of appointment.
                        </Text>
                    </View>
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'flex-start',
                            marginVertical: 10,
                            justifyContent: 'center',
                        }}
                    >
                        <DatePicker
                            style={{
                                width: '70%',
                                marginHorizontal: 10,
                            }}
                            date={this.state.date}
                            mode="date"
                            placeholder="Select Date"
                            format="YYYY-MM-DD"
                            minDate="2016-05-01"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            showIcon={false}
                            onDateChange={(date) => {this.setState({date: date})}}
                        />
                    </View>
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'flex-start',
                            marginVertical: 10,
                            justifyContent: 'center',
                        }}
                    >
                        <DatePicker
                            style={{
                                width: '70%',
                                marginHorizontal: 10,
                            }}
                            date={this.state.time}
                            mode="time"
                            placeholder="Select Time"
                            format="LT"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            showIcon={false}
                            onDateChange={(time) => {this.setState({time: time})}}
                        />
                    </View>
                </View>
            </View>
        );
    }
};