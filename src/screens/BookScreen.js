/**
 * Created by sanket.thanvi
 * 27 April 2019
 */
import React from 'react';
import {
    createBottomTabNavigator,
    createAppContainer,
    createStackNavigator,
} from 'react-navigation';
import { 
    Button, 
    View, 
    Text,
    ScrollView,
    TextInput
} from 'react-native';
import Fuse from 'fuse.js';

import datajson from '../data.json';

const BookScreenItem = (item, props) => {
    const { name, address, place, state } = item;
    return (
        <>
        <View
            style={{ 
                width: '100%',
                display: 'flex',
                flexDirection: 'row',
                justifyContent:'center',
                marginBottom: 2,
                marginTop: 2,
                // borderTopColor: 'grey',
                // borderTopWidth: 2,
                paddingRight: 0,
                backgroundColor: 'white'
            }}>
            <Text
                style={{ 
                    fontSize: 15,
                    fontWeight: 'bold',
                    marginBottom: 2,
                    flex: 1 ,
                    padding: 15,
                    paddingRight: 0
                }}
            >
                {name}
            </Text>
            <View style={{ flexDirection: 'column', paddingRight: 15, padding: 15, paddingLeft: 0, }}>
                <Text style={{ fontSize: 12, marginBottom: 1, alignSelf: 'flex-end' }}>{address}</Text>
                <Text style={{ fontSize: 12, marginBottom: 1, alignSelf: 'flex-end' }}>{place}. {state}</Text>
            </View>
            <View
                style={{
                    paddingVertical: 5, 
                    paddingHorizontal: 5, 
                    backgroundColor: 'blue',
                    height: '100%'
                }}
            >
                <Button
                    title="Book"
                    onPress={() => props.screenProps.navigation.navigate('BookModal', { item })}
                    color="white"
                />
            </View>
        </View>
        </>
    );
}

export default class BookScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            sellers: datajson.sellers || []
        }
        const options = {
            threshold: 0.1,
            keys: [
            "name", "address", "place", "state"
            ]
        };
        const fuse = new Fuse(datajson.sellers || [], options); // "list" is the item array
        this.fuse = fuse;
    }

    readBookData = () => {
        // const { screenProps } = this.props;
        // const { fire } = screenProps;
        // fire.database().ref('sellers/').on('value', (snapshot) => {
        //     const data = snapshot.val();
        //     this.setState({
        //         sellers: data,
        //     });
        // });
    }

    componentWillMount() {
        
        // this.readBookData();
    }

    render() {
        const { sellers } = this.state;
        // console.log('sellers:::', this.props);

        return (
            <View style={{ flex: 1, paddingTop: 40, paddingBottom: 10, }}>
                <TextInput
                    onChangeText={(text) => {
                        if (!text || text.length === 0) {
                            this.setState({
                            sellers: datajson.sellers || []
                            })
                        } else {
                            this.setState({
                            sellers: this.fuse.search(text) || datajson.sellers || []
                            })
                        }
                    }}
                    placeholder='Search'
                    style={{
                        fontSize: 15,
                        fontWeight: '500',
                        textAlign: 'center',
                        borderBottomColor: 'grey',
                        borderBottomWidth: 1,
                        borderColor: 'grey',
                        borderWidth: 1,
                        margin: 6,
                        paddingVertical: 5,
                    }}     
                />             
                <ScrollView contentContainerStyle={{  backgroundColor: '#e5e5ff', flex: 1, justifyContent: 'flex-start', flexDirection: 'column', alignItems: 'center' }}>
                {sellers.map(item => BookScreenItem(item, this.props))}
                </ScrollView>
            </View>
        );
    }
}
  