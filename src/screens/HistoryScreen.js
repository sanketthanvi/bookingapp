/**
 * Created by sanket.thanvi
 * 27 April 2019
 */

import React from 'react';
import {
    createBottomTabNavigator,
    createAppContainer,
    createStackNavigator,
} from 'react-navigation';
import { TextInput, View, Text, ScrollView } from 'react-native';
import datajson from '../data.json';
import Fuse from 'fuse.js';

const ScreenItem = item => {
  const { name, address, place, state, date, time } = item;
  return (
    <>
    <View style={{
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        // borderTopColor: 'grey',
        // borderTopWidth: 2,
        backgroundColor: 'white',
        marginTop: 5,
        padding: 15,
        paddingBottom: 0,
    }}>
      <Text style={{  flex: 1, fontWeight: 'bold', color: 'maroon', fontSize: 15 }}>{date} {time}</Text>
      <Text style={{  fontWeight: 'bold', color: 'maroon', fontSize: 12, alignSelf: 'flex-end' }}>Done</Text>
    </View>
    <View
      style={{ 
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent:'center',
        marginBottom: 2,
        // borderTopColor: 'grey',
        // borderTopWidth: 2,
        paddingRight: 0,
        backgroundColor: 'white'
      }}
    >
      <Text
          style={{ 
              fontSize: 15,
              fontWeight: 'bold',
              marginBottom: 2,
              flex: 1 ,
              padding: 15,
              paddingRight: 0
          }}
      >
          {name}
      </Text>
      <View style={{ flexDirection: 'column', paddingRight: 15, padding: 15, paddingLeft: 0, }}>
          <Text style={{ fontSize: 12, marginBottom: 1, alignSelf: 'flex-end' }}>{address}</Text>
          <Text style={{ fontSize: 12, marginBottom: 1, alignSelf: 'flex-end' }}>{place}. {state}</Text>
      </View>
    </View>
    </>
  );
}

export default class HistoryScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      history: datajson.history || []
    }
    const options = {
      threshold: 0.1,
      keys: [
        "name", "address", "place", "state"
      ]
    };
    const fuse = new Fuse(datajson.history || [], options); // "list" is the item array
    this.fuse = fuse;
  }

  render() {
    const { history } = this.state;
    return (
      <View style={{ flex: 1, paddingTop: 40, paddingBottom: 10, }}>
        <TextInput
          onChangeText={(text) => {
            if (!text || text.length === 0) {
              this.setState({
                history: datajson.history || []
              })
            } else {
              this.setState({
                history: this.fuse.search(text) || datajson.history || []
              })
            }
          }}
          placeholder='Search'
          style={{
            fontSize: 15,
            fontWeight: '500',
            textAlign: 'center',
            borderBottomColor: 'grey',
            borderBottomWidth: 1,
            borderColor: 'grey',
            borderWidth: 1,
            margin: 6,
            paddingVertical: 5,
          }}     
        />          
        <ScrollView contentContainerStyle={{  backgroundColor: '#e5e5ff', flex: 1, justifyContent: 'flex-start', flexDirection: 'column', alignItems: 'center' }}>
          {history.map(item => ScreenItem(item, this.props))}
        </ScrollView>
      </View>
    );
  }
}
