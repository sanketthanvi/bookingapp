/**
 * Created by sanket.thanvi
 * @format
 * @flow
 */

import React from 'react';
import Login from './src/Login';
import Viewer from './src/Viewer';

export default class App extends React.Component {

  state = {
    loggedIn: false,
  };

  render() {

    const { loggedIn } = this.state;

    if (!loggedIn) {
      return (
        <Login 
          afterLogin={() => {
            this.setState({
              loggedIn: true,
            });
        }}/>
      )
    }
    
    return (
      <Viewer
        signOut={() => {
          this.setState({ loggedIn: false })
        }}
      />
    );
  }
}
