27 April 2019
# Appointment Booking App

This repository contains code of Appointment booking app for Tazweed interview test. This is a very basic app using local dummy data to showcase proficiency for React-Native and JS.
This repo is tested on ios.

How to run:
1. install node
2. `npm i`
3. `npm install -g react-native-cli`
4. `react-native run-ios`

On load, there is a **Login** button.
When user click Login, it takes user to Home.
There are two options in slide drawer menu - Home (default) and Profile.

A. Profile:
Shows profile picture, email and phone number, and a **Sign Out** button.


B. Home:
In this screen, we can see three bottom tab menus - Book (default), Upcoming and History. All three tab screens have search options which works for given local dummy data.
1. Book: Renders all available service provider with name and address. If user click on "Book", it makes open a modal with respective service provider name and ask for date-time.
2. Upcoming : Renders all upcoming appointments.
3. History : Renders all previous appointments.

by Sanket Thanvi

Thank you for the opportunity.
